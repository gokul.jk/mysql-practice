create database colleg;
show databases;
drop database colleg;
create database college;
use college;
create table students(
Id int not null auto_increment,
SName varchar(50) not null,
Age int not null,
primary key (Id)
); 
show tables;
describe students;
alter table students add Gender varchar(6) not null after age;
describe students;
alter table students add City varchar(10) not null, add Contact varchar(30) not null;	
describe students;
alter table students modify Contact varchar(10) not null;
describe students;
alter table Students rename to studentinfo;
select *from studentinfo;
insert into studentinfo values(null,'Periyasamy',25,'Male','Pudukottai',99999);
insert into studentinfo values(null,'Gunaseelan',23,'Male','Pondicherry',88888);
insert into studentinfo values(null,'Arun',22,'Male','Tiruvannamalai',77777);
insert into studentinfo values(null,'Gokul',25,'Male','Karur',66666);
alter table studentinfo modify City varchar(20) not null;
insert into studentinfo (SName,Age,Gender,City,Contact) values ('Sathya',25,'Male','Karur',11111),('Santhosh',25,'Male','Chennai',22222);
 delete from studentinfo where Id=5;
 update studentinfo set City='KARUR',Contact=33333 where id=1;
 describe studentinfo;
 select *from studentinfo;
 insert into studentinfo (SName,Age,Gender,City,Contact) values ('Harii',25,'Male','Dharapuram',12121),('naveen',25,'Male','erode',24242);
select SName,Age from studentinfo;
SELECT SName, Age FROM studentinfo WHERE City = 'karur';
select *from studentinfo;
SELECT SName, Age FROM studentinfo WHERE City = 'karur' and Age>=23;
select *from studentinfo;
select Id,SName,Age,City from studentinfo where (City='karur' or City='Chennai') and Age>=25 order by City

select *from studentinfo;
delete from studentinfo where Id=6;
select *from studentinfo;
select count(City) from studentinfo
commit;
select *from studentinfo;
select count(distinct City) from studentinfo;
select *from studentinfo limit 0,5;
select *from studentinfo limit 6,10;
select *from studentinfo;
select *from studentinfo limit 0,1;
select *from studentinfo order by Id desc limit 0,1;
select max(Age) from studentinfo;
select min(Age) from studentinfo;
select avg(Age) from studentinfo;
select Gender,count(Id) from studentinfo group by Gender;
insert into studentinfo (SName,Age,Gender,City,Contact) values ('sathish',24,'Male','somur',12626),
('dass',26,'Male','coimbatore',24564),
('mohan',23,'male','kerala',46563),
('thillai',26,'male','senjeri',43422),
('bala',25,'male','tirupur',98353),
('manohar',25,'male','avinasi',69321);
select *from studentinfo;
select City,count(Id) from studentinfo group by City;
select SName from studentinfo where SName like 'sa%';
select *from studentinfo where City not in('karur');
select SName,age from studentinfo where age between 24 and 25;
create table emp (
Id integer unsigned not null auto_increment,
Ename varchar(20) not null,
role varchar(20) not null,
DOJ date not null,
primary key(Id)
);
show tables;
insert into emp (Ename,role,DOJ) values ('Sam','developer','2015-07-11'),
('arun','tester','2015-05-18'),
('guna','HR','2015-05-20'),
('mathivanan','sales','2015-05-13'),
('pandy','sales','2015-05-23');
select *from emp;
create table salary(
sid int not null auto_increment,
id int not null,
sdate date not null,
amount int not null,
primary key(sid)
);
select *from salary;
truncate table salary;
insert into salary (id,sdate,amount) values (1,'2015-06-01',10000),
(2,'2015-06-01',12000),
(3,'2015-06-01',8000),
(4,'2015-06-01',11000);
desc salary;
alter table  salary add ddd date;
update salary set ddd='2023-04-23';
select * from salary;

truncate salary;
drop table salary;
create table salary(
sid int not null auto_increment,
id int not null,
sdate date not null,
amount int not null,
primary key(sid)
);
select*from salary;
insert into salary (id,sdate,amount) values (1,'2015-06-01',10000),
(2,'2015-06-01',12000),
(3,'2015-06-01',8000),
(4,'2015-06-01',11000);
select emp.ename,emp.role,salary.sdate,salary.amount 
from emp inner join salary
on emp.id=salary.id;
 update salary set id=101 where sid=1;
 update salary set id=102 where sid=2;
 update salary set id=103 where sid=3;
 update salary set id=104 where sid=4;
 select *from salary;
 select emp.ename,emp.role,salary.sdate,salary.amount 
from emp inner join salary
on emp.id=salary.id;
select *from salary;
select *from emp;
select emp.ename,emp.role,salary.sdate,salary.amount
from emp inner join salary
on emp.id=salary.sid;
select emp.ename,emp.role,salary.sdate,salary.amount
from emp left join salary
on emp.id=salary.sid;

select emp.ename,emp.role,salary.sdate,salary.amount
from emp right join salary
on emp.id=salary.sid;
select *from studentinfo;
select SName,City,
(
case
when City='karur' then 200
when city='chennai'then 400
when city='tiruvannamalai' then 300
when city='dharapuram' then 350
else 0
end
)as distance from studentinfo;
select count(*)total from studentinfo; 

select count(*) 
from studentinfo 
where age=25;

select *from studentinfo;
update studentinfo set age=25;
select sum(age)
from studentinfo
where city='karur';
select sname,char_length(sname)charcount
from studentinfo;
select *from salary;
select sid,concat('Rs.',format(amount,0)) salaryy
from salary;
alter table studentinfo add column DOJ date;
select *from studentinfo;

update studentinfo set DOJ='2020-04-03';
select now();
select date_format(curdate(),'%d/%m/%y') date;
select *from salary;
select*from emp;
select*from studentinfo;

select City,count(Id)
from studentinfo
group by City
Having count(Id)>1
order by City;
select 	*from emp;
create table branch(
branch_id int primary key auto_increment,
branch_name varchar(20) not null,
address varchar(20));

create table emp1(
emp_id int primary key auto_increment,
ename varchar(20) not null,
jrole varchar(20) not null,
branch_id int ,
constraint fk_branchid foreign key(branch_id) references branch(branch_id) on delete set null
);
select *from emp1;
use college;

select *from emp1;
delete from emp1 where branch_id=1;
select *from branch;
insert into emp1 (ename,jrole,branch_id) values ('akhil','developer',4),
('ashok','tester',5),
('arjun','analyst',5);
insert into branch (branch_name,address) values ('chennai','velacherry'),
('madurai','sellur'),
('karur','nerur'),
('coimbatore','avinasi'),
('trichy','kknagar');
alter table branch rename column branch_name to branch_location;

alter table emp1 drop branch_id;
drop table emp1;
drop table branch;
create table emp1(
emp_id int primary key auto_increment,
ename varchar(20) not null,
jrole varchar(20) not null
);
select *from emp1;
select *from branch;
alter table emp1 add branch_id int;
alter table emp1 drop branch_id;
show index from branch;
show index from branch;
create index branch_index on branch(branch_location);
delete from branch where branch_id=1;
delete from branch where branch_name='trichy'; 
delete from emp1 where branch_id=1;
use college;
select emp1.ename,emp1.jrole,branch.branch_name,branch.address
from emp1 join branch  on emp1.branch_id=branch.branch_id;

select emp1.ename,emp1.jrole,branch.branch_name,branch.address
from emp1 cross join branch  on emp1.branch_id=branch.branch_id;

select *from emp1 where branch_id=(select branch_id from branch 
where branch_name='chennai');

select emp_id,ename
from emp1
where exists(
select *from emp1
where jrole="developer"
);
create or replace view emp_br
as
select emp_id,ename
from emp1
where exists(
select *from branch
where jrole="tester" and emp1.branch_id=branch.branch_id
order by ename); 
select *from emp_br;
select *from emp1;
select *from studentinfo;
select *from branch;
show tables;
use college;
delimiter $$
create procedure getbranchh()
begin
select* from branch;
end $$getbranchh
delimiter ;
call getbranchh();

delimiter $$
create procedure empcount1()
begin
declare total int default 0;
select count(emp_id) 
into total 
from emp1;
select total;
end $$

call empcount1(); 

delimiter $$
create procedure empcount2(
in jobrole varchar(20)
)
begin
declare total int default 0;
select count(emp_id) 
into total 
from emp1
where jobrole=jrole;
select total;
end $$
delimiter ;

call empcount2('developer');
call empcount2('analyst');

delimiter $$
create procedure getplace(
in id int)
begin
if id=1 or id=2 then 
select 'South';
else
select 'North';
end if;
end $$
delimiter ;

call getplace(4);
call getplace(2);

delimiter $$
create procedure getplace1(
in id int)
begin
case id
when 1 then 
select 'South';
when 2 then
select 'North';
else
select 'West';
end case;
end $$
delimiter ;

call getplace1(4);

create table empbckp(
emp_id int primary key auto_increment,
ename varchar(20) not null,
jrole varchar(20) not null,
branch_id int ,
constraint fk_branchidd foreign key(branch_id) references branch(branch_id) on delete set null
);

delimiter $$
create procedure emp2()
begin
declare done int default 0;
declare emp_id,branch_id int;
declare ename,jrole varchar(20);
declare cur cursor for select *from emp1;
declare exit handler for not found set done=1;
delete from empbckp; 
open cur;
label:loop
if done=1 then 
leave label;
end if;
fetch cur into emp_id,ename,jrole,branch_id;
insert into empbckp
values (emp_id,ename,jrole,branch_id);
end loop;
close cur;
end$$
delimiter ;

call emp2();
select *from empbckp;